from google.cloud import pubsub_v1
import settings

def callback(message: pubsub_v1.subscriber.message.Message) -> None:
#    print(f"Received {message}.")
    message.ack()
    if message != "":
        settings.response = message
