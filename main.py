from concurrent.futures import TimeoutError
from google.cloud import pubsub_v1
import json
import discord
from datetime import datetime
import pytz
from google.cloud import pubsub_v1
from discord import Webhook, RequestsWebhookAdapter
from callback_pubsub import callback
from webhook_discord import webhook_send
import settings


def discord_alert(event, context):
    settings.init()
    project_id = "etus-media-prod"
    subscription_id = "sre.alerts-discord"
    #credentials = service_account.Credentials.from_service_account_file('sre-automation@etus-media-development.iam.gserviceaccount.com.json')
    #scoped_credentials = credentials.with_scopes(['https://www.googleapis.com/auth/cloud-platform'])
    # Number of seconds the subscriber should listen for messages
    timeout = 5.0
    subscriber = pubsub_v1.SubscriberClient()
    subscription_path = subscriber.subscription_path(project_id, subscription_id)

    streaming_pull_future = subscriber.subscribe(subscription_path, callback=callback)
    print(f"Listening for messages on {subscription_path}..\n")
    # Wrap subscriber in a 'with' block to automatically call close() when done.
    with subscriber:
        try:
            # When `timeout` is not set, result() will block indefinitely,
            # unless an exception is encountered first.
            streaming_pull_future.result(timeout=timeout)

        except TimeoutError:
            streaming_pull_future.cancel()  # Trigger the shutdown.
            streaming_pull_future.result()  # Block until the shutdown is complete.
    if settings.response != 'none':
        details = json.loads(settings.response.data)
        incident_id = details['incident']['incident_id']
        incident_time = details['incident']['started_at']
        incident_time = datetime.fromtimestamp(incident_time, tz= pytz.timezone('America/Sao_Paulo'))
        incident_time = incident_time.strftime('%Y-%m-%d_%H:%M:%S')
        state = details['incident']['state']
        url_metric = details['incident']['url']
        description_indicent = details['incident']['summary']
        labels = details['incident']['resource']['labels']
        policy = details['incident']['policy_name']
        condition = details['incident']['condition_name']
        documentation = details['incident']['documentation']['content']
        if state == 'open':
            alert_color=0xff0000
        else:
            alert_color=0x00ff1e
        print(details)
        embed=discord.Embed(title="Incident "+state, url=url_metric, description=description_indicent, color=alert_color)
        embed.set_author(name="GCP Alert")
        embed.add_field(name="Labels", value=labels, inline=False)
        embed.add_field(name="Incident ID", value=incident_id, inline=True)
        embed.add_field(name="Started at", value=incident_time, inline=True)
        if state == 'closed':
            incident_time_close = details['incident']['ended_at']
            incident_time_close = datetime.fromtimestamp(incident_time_close, tz= pytz.timezone('America/Sao_Paulo'))
            incident_time_close = incident_time_close.strftime('%Y-%m-%d_%H:%M:%S')
            embed.add_field(name="Closed at", value=incident_time_close, inline=True)
        embed.add_field(name="Policy Name", value=policy, inline=True)
        embed.add_field(name="Condition Name", value=condition, inline=True)
        embed.add_field(name="Documentation", value=documentation, inline=False)

        webhook_send(embed)

